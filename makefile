CXX=g++

CXXFLAGS= -std=c++0x -fopenmp -D__GXX_EXPERIMENTAL_CXX0X__ -O3 -Wall -c -fmessage-length=0

all: WENO

./src/Interpolation.o: ./src/Interpolation.cpp ./src/Interpolation.hpp ./src/Matrix.hpp ./src/Vector.hpp ./src/Tensor.hpp ./src/Util.hpp ./src/Grid.hpp
	${CXX} ${CXXFLAGS} -o "src/Interpolation.o" "./src/Interpolation.cpp"
./src/WENO.o: ./src/WENO.cpp ./src/WENO.hpp ./src/Matrix.hpp ./src/Vector.hpp ./src/Tensor.hpp ./src/Util.hpp ./src/Grid.hpp
	${CXX} ${CXXFLAGS} -o "src/WENO.o" "./src/WENO.cpp"
./src/main.o: ./src/main.cpp 
	${CXX} ${CXXFLAGS} -o "src/main.o" "./src/main.cpp"
WENO: ./src/main.o ./src/WENO.o ./src/Interpolation.o
	${CXX} -fopenmp  -o "WENO"  ./src/Interpolation.o ./src/WENO.o ./src/main.o  -lm
run: WENO
	export OMP_NUM_THREADS=2 && ./WENO
test:

clean:
	rm -rf  ./src/Interpolation.o ./src/WENO.o ./src/main.o  ./src/Interpolation.d ./src/WENO.d ./src/main.d  WENO
