/*
 * mortontest.cpp
 *
 *  Created on: Apr 12, 2014
 *      Author: zrt
 */
#include <malloc.h>
#include <stdint.h>
#include "../src/mortonid.h"



int main(){
	double *anchor=(double *)malloc(2*sizeof(double));
	anchor[0]=0.333333333333,anchor[1]=0.111111111111111;

	morton_id midspec;

	uint64_t mid;

	mid = midspec.id (30,anchor);

	midspec.print(mid);printf("\n");



	uint64_t ans;
	ans=midspec.get_ancestor(mid, 10);

	uint64_t lowerbound,upperbound;
	lowerbound=ans-(uint64_t)10;
	upperbound=lowerbound+((uint64_t)1 << (64-2*10));

	printf("ans:");midspec.print(ans);printf("\n");
	printf("low:");midspec.print(lowerbound);printf("\n");
	printf("upp:");midspec.print(upperbound);printf("\n");

	uint64_t *ans_sib=(uint64_t*)malloc(3*sizeof(uint64_t));

	midspec.get_ancestor_sibling(mid, 10, ans_sib);

	for (int i=0;i<3;i++){
		midspec.print(ans_sib[i]);printf("\n");
	}

	free(ans_sib);

	anchor[0]=0.333333333333,anchor[1]=0.111111211111111;

	uint64_t mid2;

	mid2 = midspec.id (30, anchor);

	uint64_t lca;
	lca=midspec.last_common_ancestor(mid,mid2);

	midspec.print(mid);printf("\n");
	midspec.print(mid2);printf("\n");
	midspec.print(lca);printf("\n");

	midspec.print(mid);printf("\n");
	int num=midspec.get_numbering(mid, 2);

	printf("%d\n",num);

	free (anchor);

	anchor=midspec.mid2double(mid);

	printf("[%f,%f]\n",anchor[0],anchor[1]);

	free(anchor);

	return 1;
}



