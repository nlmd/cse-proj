/*
 * utility.cpp
 *
 *  Created on: Apr 15, 2014
 *      Author: zrt
 */

#include "utility.h"

void* unique_merge_sorted(void *a,int sa,void *b,int sb, size_t h,int (*cmp)(const void*,const void*),int &sm){
//	printf("unique_merge_sorted\n");
	if (sa==0&&sb==0){
		sm=0;
		return NULL;
	}

	char *cm=(char *)malloc((sa+sb)*h),*ca=(char*)a,*cb=(char*)b;
	int ia,ib,im,comp;

	ia=0;ib=0;im=0;
	while (ia<sa&&ib<sb){
		comp=cmp((void *)(ca+ia*h),(void *)(cb+ib*h));
		if (comp<0){
			memcpy((void *)(cm+im*h),(const void *)(ca+ia*h),h);
			ia++;
			while (cmp((void *)(cm+im*h),(void *)(ca+ia*h))==0)
				ia++;
			im++;
		}else if (comp>0){
			memcpy((void *)(cm+im*h),(const void *)(cb+ib*h),h);
			ib++;
			while (cmp((void *)(cm+im*h),(void *)(cb+ib*h))==0)
				ib++;
			im++;
		}else{
			memcpy((void *)(cm+im*h),(const void *)(ca+ia*h),h);
			ia++;ib++;
			while (cmp((void *)(cm+im*h),(void *)(ca+ia*h))==0)
				ia++;
			while (cmp((void *)(cm+im*h),(void *)(cb+ib*h))==0)
				ib++;
			im++;
		}
	}
	if (ia<sa){
		memcpy((void *)(cm+im*h),(const void *)(ca+ia*h),h*(sa-ia));
		im+=sa-ia;
	}else if (ib<sb){
		memcpy((void *)(cm+im*h),(const void *)(cb+ib*h),h*(sb-ib));
		im+=sb-ib;
	}
	sm=im;
	if ( sm < (sa+sb) ){
		cm=(char *)realloc((void *)cm,sm*h);
	}

	return (void *)cm;
}

void* fetch_sub_id(void *a,int sa,size_t h,int *idx,int nidx){
//	printf("fetch_sub_id:a:%0lx,sa:%d,h:%0lx,idx:%0lx,nidx:%0lx\n",a,sa,h,idx,nidx);
	if (sa==0||nidx==0)
		return NULL;
	char *csub=(char *)malloc(nidx*h);
	char *ca=(char *)a;
	int is;

	for(is=0;is<nidx;is++){
		memcpy((void *)(csub+is*h),(const void *)(ca+idx[is]*h),h);
	}

	return (void *)csub;
}

void* ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			return (void*)mid;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (l<0 || l>= num){
		return NULL;
	}
	mid=(char*)base+l*size;
	return (void*)mid;
}

void* floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			return (void*)mid;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (r<0 || r>= num){
		return NULL;
	}
	mid=(char*)base+r*size;
	return (void*)mid;
}

int id_ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			return m;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (l<0 || l> num){
		return -1;
	}
	return l;
}

int id_floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			return m;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (r<0 || r> num){
		return -1;
	}
	return r;
}

int last_floor_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
//	printf("last:\n");
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			do{
//				printf("last:%d\n",m);
			m++;
			mid=(char*)base+m*size;
			comp=compar(key,(const void*)mid);
			}while(comp==0 && m<num);
			return m-1;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (r<0 || r> num){
		return -1;
	}
	return r;
}

int first_ceil_bsearch(const void* key, const void* base, size_t num, size_t size, int (*compar)(const void*,const void*)){
	printf("first_ceil_bsearch\n");
	int l,r,m,comp;
	char *mid;
	l=0;r=num-1;

	while(l<=r){
		m=(l+r)/2;
		mid=(char*)base+m*size;
		comp=compar(key,(const void*)mid);
		if (comp==0){
			do{
				printf("%d\n",m);
			m--;
			mid=(char*)base+m*size;
			comp=compar(key,(const void*)mid);
			}while(comp==0 && m>-1);
			return m+1;
		}else if(comp<0){
			r=m-1;
		}else{
			l=m+1;
		}
	}
	if (l<0 || l> num){
		return -1;
	}
	return l;



}

int intcompare(const void *a,const void *b){
	int *ia=(int *)a,*ib=(int *)b;

	if (*ia<*ib)
		return -1;
	else if (*ia>*ib)
		return 1;
	else
		return 0;

}

int mid_id_compare(const void *a, const void *b){
	uint64_t *ua=(uint64_t*)a, *ub=(uint64_t*)b;

	if (*ua<*ub)
		return -1;
	else if (*ua>*ub)
		return 1;

	int *ia=(int *)((char*)a+sizeof(uint64_t)),*ib=(int *)((char*)a+sizeof(uint64_t));
	if (*ia<*ib)
		return -1;
	else if (*ia>*ib)
		return 1;
	else
		return 0;
}

int* mid_id_fetch_id(char* midids, int no){
	if (no==0)
		return NULL;

	size_t h1=sizeof(uint64_t),h2=sizeof(int);
	size_t midids_size=h1+h2;
	int *ids=(int*)malloc(no*sizeof(int));
	int i;

	for (i=0;i<no;i++){
		memcpy((void *)(ids+i),(const void*)(midids+i*midids_size+h1),h2);
	}
	return ids;
}

int get_omp_num_threads(){
	char *th_num;
  	int t_num;
	th_num=getenv("OMP_NUM_THREADS");
	if (th_num!=NULL){
		t_num=atoi(th_num);
	}else{
		t_num=1;
	}
	return t_num;
}

void qsort_merge_par(void* base, size_t num, size_t size,int (*compar)(const void*,const void*)){
	size_t t_num=(size_t)get_omp_num_threads();

//	qsort(base,num,size,compar);
	pmsort(base,num,size,compar,t_num);
}


void genericScan(void *X, int n, size_t l,void (*ubop)(void * input1andoutput, void *input2)){
	int i,j,level,power,ppower,threads;
//	double run_time;
	char *c = (char *)X;

	level=ceil(log2(double(n)));

//	run_time = omp_get_wtime();

	for (i=1;i<(level+1);i++){
		power=1 << i;
		ppower=1 << (i-1);
		threads=floor(double(n)/(double)power)+1;
		#pragma omp parallel for schedule(static)
		for(j=1;j<threads;j++){
			ubop((void*)(c+(j*power-1)*l),(void *)(c+(j*power-1)*l-ppower*l));
		}
	}

	for (i=level;i>0;i--){
		power=1 << i;
		ppower=1 << (i-1);
		threads=ceil(floor(double(n)/(double)ppower)/2);
		#pragma omp parallel for schedule(static)
		for(j=1;j<threads;j++){
			ubop((void*)(c+(j*power-1)*l+ppower*l),(void *)(c+(j*power-1)*l));
		}
	}

//	run_time = omp_get_wtime() -run_time;

//	printf("Finished in %4.2lf.\n", run_time);
	// for (i=1;i<n;i++)
	// 	ubop( (void *)(c+i*l),  (void *)(c+l*(i-1)) );
}

void userBinaryOperatordouble3d( void *x1, void *x2){
	double *a = (double *) x1;
	double *b = (double *) x2;
	int i;

	for (i=0;i<3;i++){
		*(a+i) += *(b+i);
	}
}

void userBinaryOperatorint( void *x1, void *x2){
	int *a = (int *) x1;
	int *b = (int *) x2;

  *a += *b;
}

void msort (void* base, size_t num, size_t size,
            int (*compar)(const void*,const void*)){
  	if (num <= MERGE_LIM){
  		// std::cout << "qsort" << std::endl;
  		qsort(base, num, size, (*compar));
  	}
	else
	{
		if (size <= 1) return;
		// std::cout << "split" << std::endl;
		msort(base, num/2, size, (*compar));
		msort((void*)((char*)base + (num/2)*size), num - num/2, size, (*compar));
	}

		merge(base, num, num/2, num - (num/2), size, (*compar));

}

void pmsort (void* base, size_t num, size_t size,
            int (*compar)(const void*,const void*) ,
            size_t threads){

	// size_t threads =  (size_t)omp_get_thread_num();

	// std::cout << "threads:" << threads << std::endl;

	if (threads == 1){
		msort(base, num, size, (*compar));
	}
	else if (threads > 1){
		#pragma omp parallel sections
		{
			#pragma omp section
			pmsort(base, num/2, size,(*compar), threads/2);
			#pragma omp section
			pmsort((void*)((char*)base + (num/2)*size), num - num/2, size, (*compar), threads - threads/2);
		}
	}

	merge(base, num, num/2, num - num/2, size, (*compar));
}

void merge (void* base, size_t num, size_t num1,
	size_t num2, size_t size,
    int (*compar)(const void*,const void*)){

	int i, j, k;
  	register char* a = (char*)base;
  	register char* b = a + num1*size;
  	char *c;

  	if (compar((void*)(a + (num1-1)*size), (void*)(b))<=0) return;

  	c = (char *)malloc(num2 * size);
  	memcpy(c, b, num2 * size);

 	i = num1 - 1; j = num2 - 1; k = num - 1;
	while (i >= 0 && j >= 0)
	{
		if (compar((void*)(a + i*size), (void*)(c + j*size))>0)
			// *((char*)base + (k--)*size) = *(a + (i--)*size);
			memcpy((char*)base + (k--)*size,a + (i--)*size,size);
		else
			// *((char*)base + (k--)*size) = *(c + (j--)*size);
			memcpy((char*)base + (k--)*size,c + (j--)*size,size);
	}
	while (j >= 0){
		// *((char*)base + (k--)*size) = *(c + (j--)*size);
		memcpy((char*)base + (k--)*size,c + (j--)*size,size);
	}
	free(c);

}


