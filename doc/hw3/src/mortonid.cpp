/*
 * mortonid.cpp
 *
 *  Created on: Apr 12, 2014
 *      Author: zrt
 */

#include "mortonid.h"

morton_id::morton_id():tb(64),lb(6){
	// TODO Auto-generated constructor stub

}

morton_id::~morton_id() {
	// TODO Auto-generated destructor stub
}

uint64_t morton_id::id(int level,double *anchor){
	assert(tb==64);assert(lb<tb);

	int cb, maxlevel;
	uint64_t mid=0;

	cb=tb-lb;
	maxlevel = cb/2;

	assert(level < 1 << lb);

	uint32_t ix,iy;
	ix = (uint32_t) floor ( anchor[0]*(1 << maxlevel) );
	iy = (uint32_t) floor ( anchor[1]*(1 << maxlevel) );

	if (anchor[0]==1.0){
		ix = (uint32_t)0-1;
	}
	if (anchor[1]==1.0){
		iy = (uint32_t)0-1;
	}


	uint32_t cmax;
	cmax=1 << maxlevel;
	assert(ix < cmax);assert(iy < cmax);

//	printf("ax=%1.16E,ix=%u\n ay=%1.16E,iy=%u\n",anchor[0],ix,anchor[1],iy);

	for (int i=0;i<maxlevel;i++){
		mid = mid << 1;
		mid+=(iy & ((uint64_t)1 << (maxlevel-i-1) )) >> (maxlevel-i-1);//printf("mid:%lu\n",mid);
		mid = mid << 1;
		mid+=(ix & ((uint64_t)1 << (maxlevel-i-1) )) >> (maxlevel-i-1);//printf("mid:%lu\n",mid);
	}

	uint8_t ulevel=(uint8_t)level;

	for (int i=0;i<lb;i++){
		mid = mid << 1;
		mid+=((uint64_t)ulevel & ((uint64_t)1 << (lb-i-1) )) >> (lb-i-1);
	}

	return mid;
}

void morton_id::print(uint64_t id){
	int cb=tb-lb;
	int i;

	printf(" xy:");
	for (i=0;i<cb;i++)
		printf("%1lu", (id & ((uint64_t)1 << (tb-i-1) )) >> (tb-i-1) );

	printf(" lv:");
	for (i=cb;i<tb;i++)
		printf("%1lu", (id & ((uint64_t)1 << (tb-i-1) )) >> (tb-i-1) );

}

double* morton_id::mid2double(uint64_t mid){
	double* anchor=(double *)malloc(2*sizeof(double));
	int i,maxlevel=(tb-lb)/2;
	uint64_t ux=0,uy=0;

	for (i=0;i<maxlevel;i++){
		uy+=( ( (uint64_t)1 << (tb-2*i-1) ) & mid ) >> (maxlevel-i+lb);
		ux+=( ( (uint64_t)1 << (tb-2*i-2) ) & mid ) >> (maxlevel-i-1+lb);
	}
//	printf("mid:");print(mid);printf("\n");
//	printf("ux:");print(ux);printf("\n");
//	printf("uy:");print(uy);printf("\n");
	double div=(double) ((uint64_t)1 << (maxlevel) );
//	printf("div:%f\n",div);
//	printf("ux:%f\n",(double)ux);
//	printf("uy:%f\n",(double)uy);

	double dux=(double) ux;
	double duy=(double)	uy;

	anchor[0]=1/div*(dux);
	anchor[1]=1/div*(duy);

//	printf("[%f,%f]\n",anchor[0],anchor[1]);

	return anchor;
}

uint64_t morton_id::last_common_ancestor(uint64_t first, uint64_t last){
	int total=tb;
	int maxlevel=(tb-lb)/2;
	int i,level;
	uint64_t xf,xl,yf,yl,lca;

	for (i=0,level=0,lca=0;i<maxlevel;i++){
		xf= ( ( (uint64_t)1 << total-2*i-1) & first ) >> (total-2*i-1);
		xl= ( ( (uint64_t)1 << total-2*i-1) & last ) >> (total-2*i-1);
		yf=	( ( (uint64_t)1 << total-2*i-2) & first ) >> (total-2*i-2);
		yl=	( ( (uint64_t)1 << total-2*i-2) & last ) >> (total-2*i-2);
		if (xf==xl && yf==yl){
			lca+=( ( (uint64_t)1 << total-2*i-1) & first );
			lca+=( ( (uint64_t)1 << total-2*i-2) & first );
			level++;
		}else{
			break;
		}
	}

	lca+=level;

	return lca;
}

uint64_t morton_id::get_ancestor(uint64_t des, int level){
		uint64_t ans;

		ans= des & ( ( (uint64_t)1 << 2*level) - 1 ) << (tb-2*level);
		ans+=(uint64_t)level;

		return ans;
}

void morton_id::get_ancestor_sibling(uint64_t des,int level, uint64_t *an_sib){
	uint64_t ans;

	ans=get_ancestor(des, level);

	an_sib[0]=ans ^ ((uint64_t)1 << (tb-2*level));
	an_sib[1]=ans ^ ((uint64_t)2 << (tb-2*level));
	an_sib[2]=ans ^ ((uint64_t)3 << (tb-2*level));

}

uint64_t morton_id::get_ch_lower(uint64_t mid, int level){
	uint64_t low;
	low=mid-(uint64_t)level;
	return low;
}

uint64_t morton_id::get_ch_upper(uint64_t mid, int level){
	uint64_t upp;
	upp=mid-(uint64_t)level+((uint64_t)1 << (tb-2*level))-(uint64_t)1;
	return upp;
}

int morton_id::get_numbering(uint64_t mid, int level){
	assert(level>0);
	int num;

	num = (int)((mid & ( (uint64_t)3 << (tb-2*level) )) >> (tb-2*level));

	return num;
}

int morton_id::get_lb(){
	return lb;
}

int morton_id::get_tb(){
	return tb;
}
