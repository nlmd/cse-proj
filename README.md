Parallel Semi-Lagragian with WENO and time splitting
===

Compile
---
```
make
```

Run it
---
```
export OMP_NUM_THREADS=2 && ./WENO
```

Performance and Documents
---
See Wiki. [![Build Status](https://drone.io/bitbucket.org/nlmd/cse-proj/status.png)](https://drone.io/bitbucket.org/nlmd/cse-proj/latest)

